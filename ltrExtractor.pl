###
###script to print out LTR from fasta sequence
###pulling the target range is 0 referenced so if you want 5 you want 6

#!/usr/bin/perl
use strict;
use warnings;

my $contig = $ARGV[0];
my $num1 = $ARGV[1]; #start position
my $num2 = $ARGV[2]; #end position
my $subNum = $ARGV[3]; #sub number when multiple LTRs per contig

my $header = $contig;
$header =~ s/.fa//g;

my @contig;

open (AFILE, $contig) or die "cannot open $contig file\n";
while (my $line = <AFILE>) {
	chomp $line;
	@contig = split(//, $line);
}

my @slice = @contig[$num1..$num2];
print ">$header.$subNum\n@slice\n";