#!/usr/bin/perl
use strict;
use warnings;


my $fastaFile = $ARGV[0]; #this is the modified fasta file
my $locationList = $ARGV[1]; #this is the tab delimited coordinate file

#initialize variables and arrays
my @seq;
my @locations;
my $title;
my $fasta;


open(AFILE, $fastaFile) or die "cannot open $fastaFile\n";
while(my $line = <AFILE>) {
	chomp $line;
	my @seq = split (/ /, $line);
	#print "@seq\n";
	$title = $seq[0];
	#print "$title\n";
	$fasta = $seq[1];
	#print "$fasta\n";
	open OUT, ">$title.fa";
	print OUT "$fasta\n";
}

open (BFILE, $locationList) or die "cannot open $locationList\n";
while(my $line = <BFILE>) {
	chomp $line;
	push @locations, $line;
}
close BFILE;

for (my $i=0; $i<@locations; $i++) {
	my $location = $locations[$i];
	#print "$location\n";
	my @single = split (/\t/, $location);
	my $contig = $single[0];
	my @contigFull = split (/\./, $contig);
	my $contigShort = $contigFull[0];  
	my $contigSub = $contigFull[1];
	#print "$contigShort\t$contigSub\n";
	my $start = $single[1];
	my $end = $single[2];
	print "$contigShort\t$contigSub\t$start\t$end\n";
		system("perl ltrExtractor.pl $contigShort.fa $start $end $contigSub > $contig\_full.fa");
	
}