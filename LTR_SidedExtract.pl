#!/usr/bin/perl
use strict;
use warnings;


my $fastaFile = $ARGV[0]; #this is the modified fasta file
my $locationList = $ARGV[1]; #this is the tab delimited coordinate file

#initialize variables and arrays
my @seq;
my @locations;
my $title;
my $fasta;


open(AFILE, $fastaFile) or die "cannot open $fastaFile\n";
while(my $line = <AFILE>) {
	chomp $line;
	my @seq = split (/ /, $line);
#	print "@seq\n";
	$title = $seq[0];
#	print "$title\n";
	$fasta = $seq[1];
#	print "$fasta\n";
	open OUT, ">$title.fa";
	print OUT "$fasta\n";
#	$n++;
}

open (BFILE, $locationList) or die "cannot open $locationList\n";
while(my $line = <BFILE>) {
	chomp $line;
	push @locations, $line;
}
close BFILE;

for (my $i=0; $i<@locations; $i++) {
	my $location = $locations[$i];
	#print "$location\n";
	my @single = split (/\t/, $location);
	my $contig = $single[0];
	my @contigFull = split (/\./, $contig);
	my $contigShort = $contigFull[0];  
	my $contigSub = $contigFull[1];
	#print "$contigShort\t$contigSub\n";
	my $start = $single[1];
	my $end = $single[2]; #changed this from batch2.pl
	my $side = $single[3];
	print "$contig\t$start\t$end\t$side\n";
		system("perl ltrExtractor.pl $contigShort.fa $start $end $contigSub > $side.$contig.fa");
	
}

for (my $i=0; $i<@locations; $i++) {
        my $location = $locations[$i];
        my @single = split (/\t/, $location);
        my $contig = $single[0];
        my $start = $single[1];
        my $end = $single[2];
        my $side = $single[3];
                system("cat 5.$contig.fa 3.$contig.fa > $contig\_LTR.fa");
}